# Frontend App

## Messages

- Public MQTT broker and client from [hivemq](https://www.hivemq.com/public-mqtt-broker/). MQTT client with a standart settings.
    - `ipos/client/position`: relative position of an agent to a curent root point
    - `ipos/client/root`: update a root point. Default is `{"latitude": 51.02545, "longitude": 13.72295}`.
    - `ipos/client/extracted` extracted attributes. Related with VDA5050
- Messages description: [SimpleSceneIntegration Interface](https://md.inf.tu-dresden.de/IPos_IFSimpleSceneIntegration).

### Online app

Deployed application is subscribed to similar messages but from a different broker. 
To publish messages you can use the client from hivemq but change the connection settings

- Link: https://ipos-osm.herokuapp.com/

Connection settings
- host: `broker.emqx.io`
- port: `8084`
- SSL: `X`


## Run
- 1. Install all dependency and run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
    - if you don't want to install all dependencies, you can simply run the application in a container.
        1. `docker build -t frontend-osm .`
        2. `docker run -p 4200:80 frontend-osm:latest`
        3. Navigate to `http://localhost:4200/`
- 2. Send messages to a `hivemq` public broker
    - topic `ipos/client/position`
        - new positions
            - ```json
                    {
                    "objects": [
                        {
                        "id": "Employee1",
                        "sensorId": "UWB_1",
                        "type": "HUMAN",
                        "sensorType": "UWB",
                        "position": {
                            "refSystemId": "ROOT",
                            "point": {
                            "x": 28,
                            "y": -12,
                            "z": 20
                            },
                            "accuracy": 1
                        },
                        "orientation": {
                            "x": 0,
                            "y": 0,
                            "z": -0.7071,
                            "w": 0.7071
                        },
                        "extractedAttributes": {
                            "batteryChargeLevel": 70,
                            "loadedItems": [23, 1, 25, 17],
                            "errors": [2, 1, 6],
                            "theta": -0.9
                        },
                        "lastPosUpdate": "2021-09-14T09:41:20+00:00",
                        "zoneDescriptors": [
                            {
                            "zoneId": "door_zone",
                            "notificationType": "EntryNotification"
                            }
                        ]
                        }
                    ],
                    "type": "EntryNotification"
                    }
            ```
            - More objects can be added to the `"object": []`
            - There are special colours for objects. Config: [MarkerColorList](src/environments/environment.ts)
    - topic `ipos/client/waypoint`
        - new way points
            - ```json
                [
                    {
                        "agentId":"Employee1",
                        "agentType":"HUMAN",
                        "publisher":"main",
                        "position":{
                            "refSystemId":"ROOT",
                            "point":{
                                "x":58,
                                "y":42,
                                "z":20
                            }
                        },
                        "time":"2022-04-14T09:41:20+00:00"
                    },
                    {
                        "agentId":"Employee1",
                        "agentType":"HUMAN",
                        "publisher":"main",
                        "position":{
                            "refSystemId":"ROOT",
                            "point":{
                                "x":18,
                                "y":12,
                                "z":50
                            }
                        },
                        "time":"2022-04-14T09:41:20+00:00"
                    },
                    {
                        "agentId":"Employee2",
                        "agentType":"ROBOT",
                        "publisher":"Employee2",
                        "position":{
                            "refSystemId":"ROOT",
                            "point":{
                                "x":-18,
                                "y":-52,
                                "z":33
                            }
                        },
                        "time":"2022-04-14T10:41:20+00:00"
                    }
                ]

            ```
    - topic `ipos/client/root`
        - ```json
            {
            "refSystemId": "ROOT",
            "point": {
                "latitude": 51.02535,
                "longitude": 13.72319
            },
            "accuracy": 1
            }
            ```



## Help
- This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.9.
- Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.
- Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.
- Validate JSON: [jsonformatter.org](https://jsonformatter.org/)

