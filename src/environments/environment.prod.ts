import { IMqttServiceOptions } from 'ngx-mqtt';

export const environment = {
  production: true,

  mapbox: {
    accessToken1: 'pk.eyJ1IjoidmFsYXZhbmNhIiwiYSI6ImNrbGZlNzhpaTJhb3oyeG4weHEyZ2dvaDMifQ.3CSKC5AkzxrAtQ40BD5tsQ',
    accessToken2: 'pk.eyJ1IjoidmFsYXZhbmNhIiwiYSI6ImNrbGZlYm13ZTJtZ2gyb25wbGhqdGhmZzkifQ.atRzBqwKjY9wEUlue2YLzQ'
  },

  osm: {
    apikey: 'cef83d0812574b979a9ce37273909976'
  },

  indoorequal: {
    APIkey: 'l70N1y3m02lRDrlIqOzYyB'
  },
  

};

export const MQTTconfig: IMqttServiceOptions = {
  hostname: 'broker.emqx.io',
  port: 8084,
  protocol: 'wss',
  path: '/mqtt'
};

export const MarkerColorList: MarkerColor[]  = [
  {"marker": "red", "color": "#d32f2f"},
  {"marker": "green", "color": "#008632"},
  {"marker": "blue", "color": "#0076EE"},
  {"marker": "violet", "color": "#9700ee"},
  {"marker": "yellow", "color": "#eee600"},
  {"marker": "grey", "color": "#606060"},
]

interface MarkerColor {
  marker: string
  color: string 
}

export function getMarkerConfig(counter: number) {

  if (counter < 0) {
    counter = 0
  }

  if (MarkerColorList.length <= counter) {
    counter = counter - MarkerColorList.length
  }

  return MarkerColorList[counter]
}