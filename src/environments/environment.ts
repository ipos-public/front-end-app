// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { IMqttServiceOptions } from 'ngx-mqtt';

export const environment = {
  production: false,

  mapbox: {
    accessToken1: 'pk.eyJ1IjoidmFsYXZhbmNhIiwiYSI6ImNrbGZlNzhpaTJhb3oyeG4weHEyZ2dvaDMifQ.3CSKC5AkzxrAtQ40BD5tsQ',
    accessToken2: 'pk.eyJ1IjoidmFsYXZhbmNhIiwiYSI6ImNrbGZlYm13ZTJtZ2gyb25wbGhqdGhmZzkifQ.atRzBqwKjY9wEUlue2YLzQ'
  },

  osm: {
    apikey: 'cef83d0812574b979a9ce37273909976'
  },

  indoorequal: {
    APIkey: 'l70N1y3m02lRDrlIqOzYyB'
  },

};

export const MQTTconfig: IMqttServiceOptions = {
  hostname: 'broker.hivemq.com',
  port: 8000,
  path: '/mqtt'
};

// export const SecMQTTconfig: IMqttServiceOptions = {
//   hostname: 'broker.emqx.io',
//   port: 8084,
//   protocol: 'wss',
//   path: '/mqtt'
// };

export const MarkerColorList: MarkerColor[]  = [
  {"marker": "red", "color": "#d32f2f"},
  {"marker": "green", "color": "#008632"},
  {"marker": "blue", "color": "#0076EE"},
  {"marker": "violet", "color": "#9700ee"},
  {"marker": "yellow", "color": "#eee600"},
  {"marker": "grey", "color": "#606060"},
]

interface MarkerColor {
  marker: string
  color: string 
}

export function getMarkerConfig(counter: number) {

  if (counter < 0) {
    counter = 0
  }

  if (MarkerColorList.length <= counter) {
    counter = counter - MarkerColorList.length
  }

  return MarkerColorList[counter]
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
