interface IposConfigWrapper {
  frames: IposFrameConfig[]
  objects: IposObject[]
  objectConfigs: IposObjectConfig[]
  refSystems: RefSystem[]
  monitoringRequests: IposMonitoringRequest[]
}

interface IposObjectConfig {
  agentId: string
  sensorId: string
  agentType: string
  sensorType: string
}
// note: IposObjectConfigs are used to register sensors. Beacon-positions are POIs and should be sent to sensor after registration. Bluetooth/UWB sensor need to know a reference system id. All Beacon-position sent to the Bluetooth/UWB sensor should have the same reference system id

interface IposPositionEvent {
  object: IposObject[]
  type: string
  // repeated IposZoneDescriptor zoneDescriptors = 2; // contains information about all zones that this position belongs to
}

interface IposZoneDescriptor {
  zoneId: string
  type: string
}

interface IposObject {
  id: string
  sensorId: string
  type: string
  sensorType: string
  position: IposPosition
  orientation: IposSimpleOrientation
  lastPosUpdate: string
}

interface IposFrameConfig {
  id: string
  space: IposSpace[]
}

interface RefSystem {
  id: string
  position: IposPosition
  orientation: IposSimpleOrientation
}

interface IposMonitoringRequest {
  frameId: string
  // repeated string frameIds = 1;
  delta: number
  updateFrequency: number
  type: string[]
  id: string[]
  fusionStrategy: string
  exit_notification: boolean
  properties: string[]
  monitoringTaskId: string
  requestorProtocol: string
}

interface IposMonitoringResponse {
  monitoringTaskId: string
  status: string
}

interface IposPosition {
  refSystemId: string
  point: IposPoint3D
  accuracy: number
}

interface IposPoint3D {
  x: number
  y: number
  z: number
}

interface IposSpace {
  position: IposPosition
  orientation: IposSimpleOrientation
  x: number
  y: number
  z: number
}

interface IposSimpleOrientation {
  x: number
  y: number
  z: number
  w: number
}

