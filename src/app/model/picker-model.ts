export interface PickerMessage {
    fEndInitPicklists: Picklist[]
    fEndUpdateInventoryItems: FEndUpdateInventoryItem[]
    fEndUpdateShipmentBins: ShipmentBins[]
}

export interface ShipmentBins {
    index: number
    isCorrect: boolean
    inventoryItem_Ist: string
}


export interface FEndUpdateInventoryItem {
    index: number
    isCorrect: boolean
    shipmentBinNrIst: number
}

export interface Picklist {
    picklistId: string
    pickerId: string
    fEndInitRows: InitRow[]
}

interface InitRow {
    index: number
    productId: string
    inventoryItemSoll: string
    shipmentBinNrSoll: number
}