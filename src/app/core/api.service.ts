import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';

export interface ApiError {
  code: string;
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private locationsUrl: string;

  constructor(private httpClient: HttpClient) {
    this.locationsUrl = 'http://localhost:8080/locations';
  }

  getXml(url: string): Observable<string> {
    return this.httpClient
      .get(url, {responseType: 'text'})
      .pipe(catchError(this._handleError));
  }


  public findAll(): Observable<string[]> {
    return this.httpClient.get<string[]>(this.locationsUrl);
  }

  public save(loc: string) {
    return this.httpClient.post<string>(this.locationsUrl, loc);
  }

  private _handleError(error: ApiError) {
    return throwError(error.message || 'Could not complete your request; please try again later.');
  }

}
