import { Component, OnInit, ViewChild } from '@angular/core';

import { IMqttMessage, MqttService } from 'ngx-mqtt';
import { Subscription } from 'rxjs';
import { MatTable } from '@angular/material/table';
import { NONE_TYPE } from '@angular/compiler';

import {PositionUpdate, ZoneDesc} from 'src/app/model/base-model';
import {PickerMessage} from 'src/app/model/picker-model';

var MESSAGE  = '{ \
  "fEndInitPicklists": [{\
    "picklistId": "picklist_1",\
    "pickerId": "Employee_1",\
    "fEndInitRows": [{\
      "index": 1,\
      "productId": "Rasierer",\
      "inventoryItemSoll": "invit1",\
      "shipmentBinNrSoll": 4\
    }, {\
      "index": 2,\
      "productId": "Smartphone",\
      "inventoryItemSoll": "invit2",\
      "shipmentBinNrSoll": 3\
    }, {\
      "index": 3,\
      "productId": "Bohrmaschine",\
      "inventoryItemSoll": "invit3",\
      "shipmentBinNrSoll": 3\
    }, {\
      "index": 4,\
      "productId": "Hammer",\
      "inventoryItemSoll": "invit4",\
      "shipmentBinNrSoll": 3\
    }]\
  }],\
  "fEndUpdateInventoryItems": [{\
    "index": 2,\
    "isCorrect": false,\
  "inventoryItemIst": "box_4"\
  }]\
}'

@Component({
  selector: 'pick-list',
  templateUrl: './pick-list.component.html',
  styleUrls: ['./pick-list.component.scss']
})
export class PickListComponent implements OnInit {

  @ViewChild(MatTable)
  table!: MatTable<Object>;

  private subsPosition: Subscription;

  displayedColumns: string[] = ['index', 'product', 'inventory_soll', 'inventory_ist', 'shipment_soll', 'shipment_ist'];

  tableData: any;
  pickListData: any;

  constructor(private _mqttService: MqttService) {
    this.tableData = []
    this.pickListData = {}
    
    let msg: PickerMessage = JSON.parse(MESSAGE.toString()) 

    // --- Subscribe
    this.subsPosition = this._mqttService.observe('ipos/client/tableWrapper').subscribe((message: IMqttMessage) => {
      let msg: PickerMessage = JSON.parse(message.payload.toString())
      let tableUpd = this.parsePickListMessage(msg)
      this.updateTableData(tableUpd)
      console.log(tableUpd)
      this.table?.renderRows();
    });
  }

  ngOnInit(): void {
  }

  updateTableData(updates: any) {
    Object.entries(updates).forEach(([key, info]) => {
      this.pickListData[key] = Object.assign({}, this.pickListData[key], info)
    })
    this.tableData = Object.values(this.pickListData)

  }

  parsePickListMessage(payload: PickerMessage) {
    let initItems = payload.fEndInitPicklists?.map((initList)=>{
      return new Map(initList.fEndInitRows.map((initRow: any)=>{
          initRow['picklistId'] = initList.picklistId
          initRow['pickerId'] = initList.pickerId
          return [initRow['index'], initRow]
        })
      )
    })

    let inventoryItems = payload.fEndUpdateInventoryItems?.map((item)=>{
      return new Map().set(item['index'], item)
    })

    let shipments = payload.fEndUpdateShipmentBins?.map((bin)=>{
      return new Map().set(bin['index'], bin)
    })

    let infoList = [...(initItems || []), ...(inventoryItems || []), ...(shipments || [])];

    let itemsMap: any = {}
    infoList.forEach((infoList: any)=>{
      infoList.forEach((info: any)=> {
        itemsMap[info['index']] = Object.assign({}, (itemsMap[info['index']] || []), info)
      })
    })

    return itemsMap 

  }

}
