import { Component, OnInit, ViewChild } from '@angular/core';

// --- MQTT
import { Subscription } from 'rxjs';
import { IMqttMessage, MqttService } from 'ngx-mqtt';

// --- Chart
import * as moment from "moment";
import { 
  ApexAxisChartSeries, 
  ApexChart, 
  ApexDataLabels, 
  ApexLegend, 
  ApexPlotOptions, 
  ApexXAxis, 
  ChartComponent 
} from 'ng-apexcharts';

import { getMarkerConfig } from 'src/environments/environment';
import { Agent, PositionUpdate, validateTypeAgent} from 'src/app/model/base-model';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  legend: ApexLegend;
  xaxis: ApexXAxis;
  plotOptions: ApexPlotOptions;
};

@Component({
  selector: 'chart-timeline',
  templateUrl: './chart-timeline.component.html',
  styleUrls: ['./chart-timeline.component.scss']
})
export class ChartTimelineComponent implements OnInit {
  
  @ViewChild("chart", { static: false }) chart!: ChartComponent;

  private subsPosition: Subscription;

  public chartOptions: Partial<ChartOptions> | any;

  constructor(private _mqttService: MqttService) {

    this.chartOptions = {
      series: [],
      chart: {
        height: 350,
        type: "rangeBar"
      },
      plotOptions: {
        bar: {
          horizontal: true
        }
      },
      dataLabels: {
        enabled: true,
        formatter: function(val: any) {
          var a = moment(val[0]);
          var b = moment(val[1]);
          var diff = b.diff(a, "minutes");
          return diff + " min";
        }
      },
      xaxis: {
        type: "datetime"
      },
      legend: {
        position: "top"
      }
    };


    this.subsPosition = this._mqttService.observe('ipos/client/position').subscribe((message: IMqttMessage) => {
      try {
        let upd: PositionUpdate = JSON.parse(message.payload.toString())
        upd.objects.forEach(obj => {
          validateTypeAgent(obj)
          this.addNewSeries(obj)
        });
      } catch(e) {
          console.log(e)
      }
    });

  }

  ngOnInit(): void {
  }

  addNewSeries(obj: Agent) {
    // --- Get index of the agent in series
    let name = obj.id
    let idx = this.chartOptions.series.findIndex((s: { name: string; }) => s.name === name) 
    if (idx == -1) {
      idx = this.chartOptions.series.push({"name": name, "data": []})
      idx = idx - 1
    } 

    // --- Marker
    // if (name in MarkerColorMap) {
    //   var markerConf = MarkerColorMap[name]
    //   this.chartOptions.series[idx]["color"] = markerConf?.circle
    // }
    var markerConf = getMarkerConfig(idx+1) // 0 is root
    this.chartOptions.series[idx]["color"] = markerConf?.color

    // --- Add new data
    obj.zoneDescriptors.forEach(zone => {
      // [1] lastPosUpdate is `EntryNotification`
      if (!this.chartOptions.series[idx]["data"].length || zone.notificationType?.toString() == "EntryNotification") {
        let patch = {
          "x": zone.zoneId,
          "y": [
            new Date(obj.lastPosUpdate).getTime(),
            new Date(obj.lastPosUpdate).getTime() + 1000
          ],
          "fillColor": markerConf?.color,
          "strokeColor": markerConf?.color
        }
        this.chartOptions.series[idx]["data"].push(patch)

      } else {
        // [2] lastPosUpdate is `undefined` + [3] lastPosUpdate is `ExitNotification`
        let oldData = this.chartOptions.series[idx]["data"].pop();
        let patch = {
          "x": zone.zoneId,
          "y": [
            oldData['y'][0],
            new Date(obj.lastPosUpdate).getTime()
          ],
          "fillColor": markerConf?.color,
          "strokeColor": markerConf?.color
        }
        this.chartOptions.series[idx]["data"].push(patch)
      }
    });

    // --- Update chart
    this.chartOptions.series = this.chartOptions.series.map((s: any) => Object.assign({}, s));
  }

  public ngOnDestroy() {
    this.subsPosition.unsubscribe();
  }

}
