import { Component, OnInit, ViewChild } from '@angular/core';
import { isAgent, PositionUpdate } from 'src/app/model/base-model';

import { IMqttMessage, MqttService } from 'ngx-mqtt';
import { Subscription } from 'rxjs';
import { MatTable } from '@angular/material/table';

import { getMarkerConfig} from 'src/environments/environment';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @ViewChild(MatTable)
  table!: MatTable<Object>;

  private subsPosition: Subscription;

  displayedColumns: string[] = ['agent', 'type', 'time', 'zone', 'position', 'battery', 'loadedItems', 'errors'];


  // testMessages: PositionUpdate[] = [
  //   { "objects": [{ "id": "turtlebot", "sensorId": "98:CD:AC:26:2D:18", "type": "ROBOT", "sensorType": "RFID_SCANNER", "position": { "refSystemId": "ROOT", "point": { "x": 2.1, "y": 0.4, "z": 0.5 }, "accuracy": 0.1 }, "orientation": { }, "lastPosUpdate": "2021-11-02T09:24:49.394953789+00:00", "zoneDescriptors": [{ "zoneId": "cobot1_door_zone", "notificationType": "EntryNotification" }, { "zoneId": "robolab_east", "notificationType": "EntryNotification" }] }], "type": "EntryNotification" },
  //   { "objects": [{ "id": "turtlebot", "sensorId": "UWB_1", "type": "ROBOT", "sensorType": "UWB", "position": { "refSystemId": "ROOT", "point": { "x": 3.0, "y": 1.5, "z": 3.0 }, "accuracy": 10.0 }, "orientation": { "x": 1.0, "y": 0.5, "z": 1.0, "w": 1.5 }, "lastPosUpdate": "2021-10-14T19:32:23+00:00", "zoneDescriptors": [{ "zoneId": "cobot1_door_zone", "notificationType": "EntryNotification" }, { "zoneId": "robolab_east", "notificationType": "EntryNotification" }] }], "type": "EntryNotification" },
  //   { "objects": [{ "id": "turtlebot", "sensorId": "UWB_1", "type": "ROBOT", "sensorType": "UWB", "position": { "refSystemId": "ROOT", "point": { "x": -3.0, "y": 4.5, "z": 3.0 }, "accuracy": 10.0 }, "orientation": { "x": 1.0, "y": 0.5, "z": 1.0, "w": 1.5 }, "lastPosUpdate": "2021-10-14T19:32:22+00:00", "zoneDescriptors": [{ "zoneId": "robolab_east", "notificationType": "EntryNotification" }, { "zoneId": "robolab_west", "notificationType": "EntryNotification" }] }], "type": "EntryNotification" },
  //   { "objects": [{ "id": "turtlebot", "sensorId": "UWB_1", "type": "ROBOT", "sensorType": "UWB", "position": { "refSystemId": "ROOT", "point": { "x": 3.0, "y": 4.5, "z": 3.0 }, "accuracy": 10.0 }, "orientation": { "x": 1.0, "y": 0.5, "z": 1.0, "w": 1.5 }, "lastPosUpdate": "2021-10-14T19:32:21+00:00", "zoneDescriptors": [{ "zoneId": "robolab_east", "notificationType": "EntryNotification" }, { "zoneId": "cobot1_window_zone", "notificationType": "EntryNotification" }] }], "type": "EntryNotification" },
  //   { "objects": [{ "id": "turtlebot", "sensorId": "UWB_1", "type": "ROBOT", "sensorType": "UWB", "position": { "refSystemId": "ROOT", "point": { "x": 3.0, "y": 1.5, "z": 3.0 }, "accuracy": 10.0 }, "orientation": { "x": 1.0, "y": 0.5, "z": 1.0, "w": 1.5 }, "lastPosUpdate": "2021-10-14T19:32:20+00:00", "zoneDescriptors": [{ "zoneId": "cobot1_door_zone", "notificationType": "EntryNotification" }, { "zoneId": "robolab_east", "notificationType": "EntryNotification" }] }], "type": "EntryNotification" }
  // ].map(obj => JSON.parse(JSON.stringify(obj)))

  dataSource: Object[] = []

  constructor(private _mqttService: MqttService) {
    // this.testMessages.forEach(upd => this.dataSource.push(...upd.objects))

    this.subsPosition = this._mqttService.observe('ipos/client/position').subscribe((message: IMqttMessage) => {
      try {
        let upd: PositionUpdate = JSON.parse(message.payload.toString())

        // 1. Validate
        let oldAgentNames = [...new Set(this.dataSource.map((agent: any) => agent.id))]
        let validAgents = upd.objects.filter((agent: any) => {
          return isAgent(agent) || oldAgentNames.includes(agent.id)
        });

        // 2. Extract
        this.dataSource.push(...validAgents); 
        let agentNames = [...new Set(this.dataSource.map((agent: any) => agent.id))]
        this.dataSource.forEach((agent: any)=> {
          let idx = agentNames.indexOf(agent.id) + 1
          agent.color = getMarkerConfig(idx).color
        })

        // 3. Update table
        this.table?.renderRows();
      } catch(e) {
          console.log(e)
      }

      // console.log(this.dataSource)
    });
  }

  ngOnInit(): void {
  }

}
