import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import * as L from "leaflet";
import 'leaflet-arrowheads';
import { IMqttMessage, MqttService } from 'ngx-mqtt';

import { environment, getMarkerConfig} from 'src/environments/environment';
import {
  Position, 
  PositionUpdate, 
  RelativePos, 
  WGS84, 
  validateTypePosition, 
  validateTypeAgent, 
  WayPoint,
  isWayPoint
} from 'src/app/model/base-model';


@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {

  private subsPosition: Subscription;
  private subsRoot: Subscription;
  private subsWayPoint: Subscription;

  private map!: L.Map;
  private root!: Position;
  
  agentsInfo: { [key: string]: {} } = {};
  markOverlays: { [key: string]: L.LayerGroup<any> } = {}; 
  wayPointsOverlays: { [key: string]: L.LayerGroup<any> } = {}; 
  posOverlays: { [key: string]: L.LayerGroup<any> } = {};

  constructor(private _mqttService: MqttService) {
    this.root = {"refSystemId": "ROOT", "point": {"latitude": 51.02545, "longitude": 13.72295}}

    // --- Root
    this.subsRoot = this._mqttService.observe('ipos/client/root').subscribe((message: IMqttMessage) => {
      try {
        let root = <Position>JSON.parse(message.payload.toString())
        validateTypePosition(root)
        console.log("New root: ", root)
        this.registerPoint("ROOT", {"position": root})
        this.root = root
      } catch(e) {
          console.log(e)
      }
    });

    // --- Agent position
    this.subsPosition = this._mqttService.observe('ipos/client/position').subscribe((message: IMqttMessage) => {
      try {
        let upd: PositionUpdate = <PositionUpdate>JSON.parse(message.payload.toString())
        
        upd.objects.forEach(obj => {
          validateTypeAgent(obj)
          this.registerPoint(obj.id, obj)
        });
      } catch(e) {
          console.log(e)
      }
    });


    // --- Agent waypoints
    this.subsWayPoint = this._mqttService.observe('ipos/client/waypoint').subscribe((message: IMqttMessage) => {
      try {
        let points: WayPoint[] = <WayPoint[]>JSON.parse(message.payload.toString())

        if (!(Array.isArray(points))) {
          points = [points]
        }

        points.forEach(wayPoint => {
          if (isWayPoint(wayPoint)) {
            this.registerWayPoint(wayPoint)
          }
        });
      } catch(e) {
          console.log(e)
      }
    });
  }

  ngOnInit(): void { } 

  // --- Controllers  

  ref2root(pos: RelativePos) { 
    // convert a relative position to WFG84 format
    let source = <WGS84>this.root.point
    let origin = L.latLng([source.latitude, source.longitude])
    let point = L.Projection.Mercator.project(origin)
    let newPoint = L.point([point.x + pos.x, point.y + pos.y])
    let newLatLng = L.Projection.Mercator.unproject(newPoint)
    return newLatLng
  }


  registerPoint(key: string, desc: { [key: string]: any }) {
    // --- Leyers for markers
    if (key in this.markOverlays) {
      this.markOverlays[key].clearLayers();


      // var myIconReplc = L.Icon.extend({
      //   options: {}
      // });

      // this.markOverlays[key].eachLayer(layer => {
      //   if (!(layer instanceof L.Circle)) {
      //     this.map.removeLayer(layer)
      //   }
      // });

    } else {
      this.markOverlays[key] = L.layerGroup().addTo(<L.Map>this.map)
    }

    // --- Info
    this.agentsInfo[key] = desc
    let pos = desc["position"]

    var props = undefined
    if (desc.extractedAttributes) {
      props = `<p> \
        <h3>${key}</h3>
        <strong>Charge</strong>: ${desc.extractedAttributes?.batteryChargeLevel}% <br>\
        <strong>Items</strong>: ${desc.extractedAttributes?.loadedItems} <br>\
        <strong>Errors</strong>: ${desc.extractedAttributes?.errors}\
      </p>`
    } else {
      props = `<p><h3>${key}</h3></p>`
    }

    this.addMarker(key, pos, props, desc.extractedAttributes?.theta)
  }

  registerWayPoint(wayPoint: WayPoint) {
    // empty position
    if (wayPoint.position==undefined) {
      console.log("Error: empty waypoint position.")
      return
    }

    // empty position
    if (Object.values(wayPoint.position).every((e)=>{e==undefined})) {
      console.log("Error: empty position.")
      return;
    }

    let key = wayPoint.agentId

    // --- Marker config
    // let serialNum = Object.keys(this.markOverlays).indexOf(key) // ref to markers
    // var markerConf = getMarkerConfig(serialNum)

    // --- Layer on the map
    if (!(key in this.wayPointsOverlays)) {
      this.wayPointsOverlays[key] = L.layerGroup().addTo(<L.Map>this.map)
    }

    let point = wayPoint.position.point

    if ('x' in point) {
      var globPos = this.ref2root(point)
    } else {
      var globPos = L.latLng([point.latitude, point.longitude]);
    }

    // --- Marker
    let markerDesc = `<p> \
      <h3>Waypoint</h3>
      <strong>Agent</strong>: ${wayPoint.agentId} <br>\
      <strong>Time</strong>: ${wayPoint.time} <br>
    </p>`

    let marker = L.circleMarker(globPos, {radius: 5, color: "#d97b16"}).bindPopup(markerDesc).openPopup();
    marker.addTo(this.wayPointsOverlays[key])
  }

  addMarker(key: string, pos: Position, popup: string, theta: number) {

    // empty position
    if (pos==undefined) {
      console.log("Error: empty position.")
      return
    }

    let point = pos.point

    // empty point
    if (Object.values(point).every((e)=>{e==undefined})) {
      console.log("Error: empty point.")
      return;
    }

    if ('x' in point) {
      var globPos = this.ref2root(point)
    } else {
      var globPos = L.latLng([point.latitude, point.longitude]);
    }
 
    if (key=="ROOT") {
      // Root Marker
      this.markOverlays[key].clearLayers();
      let marker = L.marker(globPos, {
        icon: L.icon({
          iconSize: [40, 40],
          iconAnchor: [21, 25],
          iconUrl: `assets/marker-ball-pink.png`,
          className: 'true-position-marker'
        })
      }).bindPopup(popup).openPopup();
      marker.addTo(this.markOverlays[key])
      
    } else {
      // --- Marker config
      let serialNum = Object.keys(this.markOverlays).indexOf(key)
      var markerConf = getMarkerConfig(serialNum)

      let marker = L.marker(globPos, {
        icon: L.icon({
          iconSize: [25, 41],
          iconAnchor: [13, 41],
          iconUrl: `assets/marker-icon-${markerConf.marker}.png`,
          iconRetinaUrl: `assets/marker-icon-2x-${markerConf.marker}.png`,
          shadowUrl: 'assets/marker-shadow.png',
          className: 'true-position-marker'
        })
      }).bindPopup(popup).openPopup();
      marker.addTo(this.markOverlays[key])

      // --- Accuracy
      if (pos?.accuracy) {
        L.circle(globPos, pos.accuracy, { color: markerConf.color}
        ).addTo(this.markOverlays[key]);
      }

      // --- Orientation
      if ('x' in point) {
        let orient: RelativePos = {
          "x": point.x + (theta ? Math.cos(theta)*3.5 : 3.5), 
          "y": point.y + (theta ? Math.sin(theta)*3.5 : 0),
          "z": point.z
        }
        let globPointOrient = this.ref2root(orient)

        // add arrow
        var arrow = L.polyline([globPos, globPointOrient], {color: markerConf.color}).arrowheads(
          {
            fill: true,
            color: markerConf.color
          }
        );
        arrow.addTo(this.markOverlays[key])

      }
    }
  
  }

  // @https://asymmetrik.com/ngx-leaflet-tutorial-angular-cli/
  // --- Layers: Define base layers so we can reference them multiple times
  streetMaps = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    detectRetina: true,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });

  wMaps = L.tileLayer('http://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png', {
    detectRetina: true,
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  });

  satelliteMaps = L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}', {
    id: 'mapbox.gis',
    attribution: 'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'
  });

  mapBoxMaps = L.tileLayer(
    'https://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token=' + environment.mapbox.accessToken2, {
      id: 'mapbox.streets-v11',
      attribution: '© <a href="https://apps.mapbox.com/feedback/">Mapbox</a> © <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    })

  cycleMaps =  L.tileLayer(
    'https://tile.thunderforest.com/cycle/{z}/{x}/{y}.png?apikey=' + environment.osm.apikey, {
    attribution: '&copy; OpenCycleMap, ' + 'Map data: opencyclemap.org',
    maxZoom: 21,
  })
 
  options = {
    layers: [this.cycleMaps],
    zoom: 19,
    center: L.latLng([51.02545, 13.72295])
  };

  onMapReady(map: L.Map) {
    
    this.map = map;

    let baseLayers = {
      'MapBox': this.mapBoxMaps,
      'Street Maps': this.streetMaps,
      'Wikimedia Maps': this.wMaps,
      'Satellite': this.satelliteMaps,
      'Cycle': this.cycleMaps
    }
    L.control.layers(baseLayers).addTo(this.map);

    let legend = new L.Control({ position: "bottomleft" });
    legend.onAdd = function () {
      var div = L.DomUtil.create("div", "legend");
      div.innerHTML += '<img style="opacity: .5" src="assets/marker-icon-grey.png"> <span> Employee </span><br>';
      div.innerHTML += '<i class="est-pos"></i> <span> Estimated position </span><br>';

      return div;
    };

    // legend.addTo(this.map);

    //-- check root
    if (this.root) {
      this.registerPoint("ROOT", {"position": this.root})
    }

  }  

  public ngOnDestroy() {
    this.subsPosition.unsubscribe();
    this.subsRoot.unsubscribe();
  }

}
