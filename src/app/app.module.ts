import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

// --- Map
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

// --- Charts
import { NgApexchartsModule } from 'ng-apexcharts';

// --- MQTT
import { MqttModule } from 'ngx-mqtt';
import { MQTTconfig } from './../environments/environment';

// --- WS
// import * as SockJS from 'sockjs-client';

// --- Modules
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';

// --- Services
import { ApiService } from "./core/api.service";
// import { StompService, StompConfig } from '@stomp/ng2-stompjs';

// --- Components
import { AppComponent } from './app.component';
import { MapComponent } from './components/map/map.component';
import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartTimelineComponent } from './components/chart-timeline/chart-timeline.component';
import { TableComponent } from './components/table/table.component';
import { PickListComponent } from './components/pick-list/pick-list.component';




@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    ChartTimelineComponent,
    TableComponent,
    PickListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SharedModule,
    CoreModule,
    LeafletModule,
    BrowserAnimationsModule,
    MqttModule.forRoot(MQTTconfig),
    NgApexchartsModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
