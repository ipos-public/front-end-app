import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// --- @angular/flex-layout : Layout for Angular applications; using Flexbox and a Responsive API
// import { FlexLayoutModule } from '@angular/flex-layout';

// --- Forms
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// --- material.angular.io : Material Design components for Angular
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatTabsModule } from '@angular/material/tabs';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTableModule } from '@angular/material/table';
import {MatChipsModule} from '@angular/material/chips';
// Animation
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [],
  imports: [
    FormsModule, 
    ReactiveFormsModule,
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatTabsModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSelectModule,
    MatCardModule,
    MatExpansionModule,
    MatSidenavModule,
    MatTooltipModule,
    MatTableModule
  ],
  exports: [
    FormsModule, 
    ReactiveFormsModule,
    CommonModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatTabsModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSelectModule,
    MatCardModule,
    MatExpansionModule,
    MatSidenavModule,
    MatTooltipModule,
    MatTableModule,
    MatChipsModule
  ]
})
export class SharedModule { }
